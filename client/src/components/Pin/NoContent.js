import React from "react";
import { withStyles } from "@material-ui/core/styles";
import PanToolIcon from '@material-ui/icons/PanTool';
import Typography from "@material-ui/core/Typography";

//no content display component display
const NoContent = ({ classes }) => (
  <div className={classes.root}>
    <PanToolIcon style={{ color: 'f44336' }} className={classes.icon} />
    <Typography
      noWrap
      component="h2"
      variant="h6"
      align="center"
      color="textPrimary"
      gutterBottom
    >
      Tag Your Spot!
    </Typography>
  </div>
);

const styles = theme => ({
  root: {
    display: "flex",
    alignItems: "center",
    flexDirection: "column",
    justifyContent: "center",
  },
  icon: {
    margin: theme.spacing.unit,
    fontSize: "80px"
  }
});

export default withStyles(styles)(NoContent);
