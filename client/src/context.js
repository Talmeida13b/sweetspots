import { createContext } from "react";

//this is the default state of the current user
const Context = createContext({
  currentUser: null,
  isAuth: false,
  draft: null,
  pins: [],
  currentPin: null
});

export default Context;
