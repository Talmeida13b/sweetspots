const mongoose = require("mongoose");

//user schema
const UserSchema = new mongoose.Schema({
  name: String,
  email: String,
  picture: String
});


module.exports = mongoose.model("User", UserSchema);
